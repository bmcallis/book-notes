import { configure } from '@storybook/react'
import { setOptions } from '@storybook/addon-options'

setOptions({
  name: 'Book Notes',
  url: 'https://gitlab.com/bmcallis/book-notes',
  showAddonPanel: false,
})

const req = require.context('../src/', true, /\.stories\.js$/)

function loadStories() {
  require('../books/all')

  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
